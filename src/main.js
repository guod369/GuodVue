// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import App from './App'

import router from './router'

import Vuex from 'vuex'
import store from './vuex/store.js'

import iView from 'iview'
import 'iview/dist/styles/iview.css';

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import axios from 'axios' // 1、在这里引入axios

import '../mock/index.js'
Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(ElementUI)
Vue.use(iView)
Vue.prototype.$axios = axios

Vue.prototype.$axios = axios.create({
  /*请求初始化信息配置*/
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>'
})

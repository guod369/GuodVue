import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Index from '@/views/index/index'
import Manage from '@/views/manage/index'
import Counter from '@/components/counter'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    }, {
      path: '/index',
      name: 'Index',
      component: Index
    }, {
      path: '/manage',
      name: 'Manage',
      component: Manage
    }, {
      path: '/counter',
      name: 'Counter',
      component: Counter
    }
  ]
})

import Mock from 'mockjs'
import login from './login'
import test from './test'

let data = [].concat(login, test)

data.forEach(function (res) {
  Mock.mock(res.path, res.data)
})

export default Mock
